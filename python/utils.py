# coding:utf-8

from itertools import *

def rwh_primes1(n):
    # http://stackoverflow.com/questions/2068372/fastest-way-to-list-all-primes-below-n-in-python/3035188#3035188
    """ Returns  a list of primes < n """
    sieve = [True] * int(n/2)
    for i in range(3,int(n**0.5)+1,2):
        if sieve[int(i/2)]:
            sieve[int(i*i/2)::i] = [False] * int((n-i*i-1)/(2*i)+1)
    return [2] + [2*i+1 for i in range(1,int(n/2)) if sieve[i]]


def get_primes(limit):
    # http://stackoverflow.com/questions/2068372/fastest-way-to-list-all-primes-below-n-in-python/3035188#3035188
    numbers = set(range(limit, 1, -1))
    while numbers:
        prime = numbers.pop()
        yield prime
        numbers.difference_update(set(range(prime * 2, limit + 1, prime)))


def prime_nums(limit=None):
    """素数ジェネレータ"""
    found = []
    for num in range(2, limit) if limit else count(2):
        if all(map(lambda x: num % x != 0, found)):
            found.append(num)
            yield num


def gcd(a, b):
    """2つの最大公倍数を計算する"""
    return a if b == 0 else gcd(b, a % b) if a > b else gcd(b, a)


def is_coprime(a, b):
    """互いに素であるかどうか"""
    return gcd(a, b) == 1
