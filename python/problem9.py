# coding:utf-8

"""
ピタゴラス数(ピタゴラスの定理を満たす自然数)とは a < b < c で以下の式を満たす数の組である.

a^2 + b^2 = c^2
例えば, 3^2 + 4^2 = 9 + 16 = 25 = 52 である.

a + b + c = 1000 となるピタゴラスの三つ組が一つだけ存在する.
これらの積 abc を計算しなさい.
"""

from itertools import *
from functools import *
from operator import *
from utils import *


def main():
    for a, b in combinations(range(1, 1001), 2):
        c = 1000 - a - b
        if (a < b < c) and (a ** 2 + b ** 2 == c ** 2):
            print(a * b * c)


if __name__ == '__main__':
    main()
