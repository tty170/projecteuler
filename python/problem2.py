# coding:utf-8

"""
フィボナッチ数列の項は前の2つの項の和である. 
最初の2項を 1, 2 とすれば, 最初の10項は以下の通りである.

1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...
数列の項の値が400万以下の, 偶数値の項の総和を求めよ.

Note:この問題は最近更新されました. お使いのパラメータが正しいかどうか確認してください.
"""


from itertools import takewhile

FIBO_NUM = 4000000

def fibo(a, b):
    while 1:
        yield a
        a, b = b, a + b

def main():
    le_four_million_fibo = takewhile(lambda x: x <= FIBO_NUM, fibo(1,2))
    even_only = filter(lambda x: x % 2 == 0, le_four_million_fibo)
    ans = sum(even_only)
    print(ans)


if __name__ == '__main__':
    main()
