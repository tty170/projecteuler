# coding:utf-8

"""
素数を小さい方から6つ並べると 2, 3, 5, 7, 11, 13 であり, 6番目の素数は 13 である.

10 001 番目の素数を求めよ.
"""

from itertools import *
from utils import *


def main():
    for c, n in enumerate(prime_nums()):
        if c == 10000:
            print(n)
            break


if __name__ == '__main__':
    main()
