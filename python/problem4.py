# coding:utf-8

"""
左右どちらから読んでも同じ値になる数を回文数という.
2桁の数の積で表される回文数のうち, 最大のものは 9009 = 91 × 99 である.

では, 3桁の数の積で表される回文数の最大値を求めよ.
"""

from itertools import product, starmap
from operator import *

def is_palindrome(num):
    num = str(num)
    half_idx = int(len(num) / 2)
    former = num[:half_idx]
    later = num[:half_idx-1:-1]
    return all(starmap(eq, zip(former, later)))


def main():
    nominate = starmap(mul, product(range(100, 999), repeat=2))
    print(max(filter(is_palindrome, nominate)))


if __name__ == '__main__':
    main()
