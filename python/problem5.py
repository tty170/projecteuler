# coding:utf-8

"""
2520 は 1 から 10 の数字の全ての整数で割り切れる数字であり, そのような数字の中では最小の値である.

では, 1 から 20 までの整数全てで割り切れる数字の中で最小の正の数はいくらになるか.
"""

from itertools import groupby, starmap
from functools import reduce
from operator import *
from utils import *


def prime_decomposition(num):
    for prime_num in prime_nums():
        if num == 1:
            break
        count = 0
        while num % prime_num == 0:
            count = count + 1
            num = num / prime_num
        if count > 0:
            yield (prime_num, count)


def main():
    decomposed = [factors  for num in range(2, 20) for factors in prime_decomposition(num)]
    # 素因数分解された各数値の中で最大の冪数を持っているものを選択
    max_exp_factors = dict(sorted(sorted(decomposed, key=lambda x: x[1]), key=lambda y: y[0])).items()
    print(reduce(mul, starmap(pow, max_exp_factors)))


if __name__ == '__main__':
    main()
