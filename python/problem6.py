# coding:utf-8

"""
最初の10個の自然数について, その二乗の和は,

1^2 + 2^2 + ... + 10^2 = 385
最初の10個の自然数について, その和の二乗は,

(1 + 2 + ... + 10)^2 = 3025
これらの数の差は 3025 - 385 = 2640 となる.

同様にして, 最初の100個の自然数について二乗の和と和の二乗の差を求めよ.
"""

from itertools import *

def first_n_square_sum(n):
    return sum(starmap(pow, product(range(1, n+1), (2,))))

def first_n_sum_square(n):
    return pow(sum(range(1, n+1)), 2)


def main():
    calc_num = 100
    print(first_n_sum_square(calc_num) - first_n_square_sum(calc_num))


if __name__ == '__main__':
    main()
