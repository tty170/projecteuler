# coding:utf-8

"""
10以下の素数の和は 2 + 3 + 5 + 7 = 17 である.

200万以下の全ての素数の和を求めよ.
"""

from itertools import *
from functools import *
from operator import *
from utils import *


def main():
    print(sum(rwh_primes1(2000000)))
    print(sum(get_primes(2000000)))
    print(sum(prime_nums(2000000)))


if __name__ == '__main__':
    main()
