# coding:utf-8

"""
13195 の素因数は 5, 7, 13, 29 である.

600851475143 の素因数のうち最大のものを求めよ.
"""


def prime_factors(num):
    for prime_num in  prime_nums():
        if num == 1:
            break
        if num % prime_num == 0:
            yield prime_num
        while num % prime_num == 0:
            num = num / prime_num


def main():
    # ウォームアップ問題
    print(list(prime_factors(13195)))
    print(max(prime_factors(600851475143)))


if __name__ == '__main__':
    main()
